SRC:= boot.s
PROTECTED:= protected_mode.s
TRGT:= boot

default: all
.PHONY: all

all:
	nasm -fbin $(SRC) -o ./$(TRGT)
	dd if=./message.txt >> ./$(TRGT)
	dd if=/dev/zero bs=512 count=1 >> ./$(TRGT)

protected:
	nasm -fbin $(PROTECTED) -o ./$(TRGT)
run:
	qemu-system-x86_64 -hda $(TRGT)
