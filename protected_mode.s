BITS 16

ORG 0x7C00

CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start

_start:
	jmp short start
	nop
times 33 db 0
start:
	jmp 0x00:actual_start

actual_start:
	;; clear interrupt
	cli
	;; set data and extra segment
	mov ax, 0x00
	mov ds, ax
	mov es, ax
	;; set stack
	mov ss, ax
	mov sp, 0x7c00
	;; set interrupt
	sti

.load_protected:
	cli
	lgdt [gdt_descriptor]
	mov eax, cr0
	or eax, 0x01
	mov cr0, eax
	jmp CODE_SEG:load32

gdt_start:
gdt_null:
	dd 0x00 	; 8 byte offset
	dd 0x00
gdt_code: 		; CS should point to this.
	dw 0xFFFF 	; Segment limit first 0-15 bits
	dw 0x00		; Base first 0-15 bits
	db 0x00		; Base 16-23 bits
	db 0x9A		; Access byte
	db 0xCF 	; 11001111b	; High bit flags and the low 5 bit flags
	db 0x00		; Base 24-31 bits
; offset 0x10
gdt_data:		; DS,SS,ES,FG,GS
	dw 0xFFFF 	; Segment limit first 0-15 bits
	dw 0x00		; Base first 0-15 bits
	db 0x00		; Base 16-23 bits
	db 0x92		; Access byte
	db 0xCF 	; 11001111b	; High bit flags and the low 5 bit flags
	db 0x00		; Base 24-31 bits
gdt_end:

gdt_descriptor:
	dw gdt_end - gdt_start - 1
	dd gdt_start

[BITS 32]
load32:
	mov ax, DATA_SEG
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	mov ebp, 0x00200000
	mov esp, ebp
	jmp $

times 510 - ($ - $$) db 0 ;; padding
dw 0xAA55 ;; boot signature
