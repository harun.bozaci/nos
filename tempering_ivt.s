BITS 16

org 0x00
_start:
	jmp short start
	nop
times 33 db 0
start:
	jmp 0x7c0:actual_start

handler_zero_division:
	mov al, 'A'
	mov ah, 0x0E
	mov bx, 0x00
	int 0x10
	iret

actual_start:
	;; clear interrupt
	cli
	;; set data and extra segment
	mov ax, 0x07c0
	mov ds, ax
	mov es, ax
	;; set stack
	xor ax, ax
	mov ss, ax
	mov sp, 0x7c00
	;; set interrupt
	sti

	;; setting interrupt handler for interrupt 0

	;; first 2 bytes offset
	mov word[ss:0x00], handler_zero_division
	;; next 2 bytes segment, since bios loads our bootable sector into 0x7C00 
	;; and very first segment we use 0x07C0, we set segment bytes to 0x7C0
	;; briefly, handler_zero_division is in 0x7c0 segment
	mov word[ss:0x02], 0x7c0

	;; implicitly executes 'int 0x00'
	;; interrupt table structure is like:
	;; OFFSET 2 BYTE
	;; SEGMENT 2 BYTE
	;; calling 'int ...' means from starting address 0x00 find interrupt handler
	;; address calculation is for 'int XXXX' 0xXXXX * 0x04

	mov ax, 0
	div ax

	lea si, [hello_world]
	call print
	jmp $

print:
	mov bx, 0
.loop:
	lodsb
	cmp al, 0
	je .done 
	call print_char
	jmp .loop
.done:
	ret

print_char:
	mov ah, 0x0E
	int 0x10
	ret

hello_world: db "Hello World", 0x0D, 0x0A, 0x00
times 510 - ($ - $$) db 0 ;; padding
dw 0xAA55 ;; boot signature
