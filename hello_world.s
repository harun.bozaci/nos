BITS 16

org 0x00
_start:
	jmp short start
	nop
times 33 db 0
start:
	jmp 0x7c0:actual_start

actual_start:
	;; clear interrupt
	cli
	;; set data and extra segment
	mov ax, 0x07c0
	mov ds, ax
	mov es, ax
	;; set stack
	xor ax, ax
	mov ss, ax
	mov sp, 0x7c00
	;; set interrupt
	sti

	lea si, [hello_world]
	call print
	jmp $

print:
	mov bx, 0
.loop:
	lodsb
	cmp al, 0
	je .done 
	call print_char
	jmp .loop
.done:
	ret

print_char:
	mov ah, 0x0E
	int 0x10
	ret

hello_world: db "Hello World", 0x0D, 0x0A, 0x00
times 510 - ($ - $$) db 0 ;; padding
dw 0xAA55 ;; boot signature
